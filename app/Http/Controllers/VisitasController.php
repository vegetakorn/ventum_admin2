<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VisitasController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {
        $fecha = date('Y-m-d');
        $fecha_actual = date("Y-m-d",strtotime($fecha."+ 1 days"));
        $ini = date("Y-m-d",strtotime($fecha."- 3 month"));
        $data['visitas']  =   DB::table('visitas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('empresas', function ($join) {
                $join->on('tiendas.empresas_Id', '=', 'empresas.Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('visitas.users_Id', '=', 'users.id');
            })
            ->select('visitas.*', 'empresas.Nombre as Empresa', 'tiendas.numsuc', 'tiendas.nombre as Tienda', 'users.name as Usuario', 'checklist.nombre as Checklist' )
            ->where('tiendas.activo', "=", 1)
            ->where('visitas.Status', "=", 131)
            ->whereBetween('visitas.HoraInicio', [$ini, $fecha_actual])
            ->orderBy('visitas.HoraInicio', 'desc')
            ->get();
        return view('visitas/lista')->with($data);
    }

    public function edit($id)
    {
        $sql = DB::table('visitas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('empresas', function ($join) {
                $join->on('tiendas.empresas_Id', '=', 'empresas.Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('visitas.users_Id', '=', 'users.id');
            })
            ->select('visitas.*', 'empresas.Nombre as Empresa', 'tiendas.numsuc', 'tiendas.nombre as Tienda', 'users.name as Usuario', 'checklist.nombre as Checklist' );
        $sql->where('visitas.Id','=', $id);
        $data['visita']  = $sql->first();
        return view('visitas/edit')->with( $data);
    }

    public function update(Request $request)
    {
            DB::table('visitas')
                ->where('visitas.Id','=',$request['id'])
                ->update([
                        'HoraInicio' => $request['ini'],
                        'HoraFin' => $request['fin'],
                        'Duracion' => $request['duracion'],
                    ]
                );
        return response()->json(['message' => "ok"] );
    }

    public function cancel(Request $request)
    {
        DB::table('visitas')
            ->where('visitas.Id','=',$request['id'])
            ->update([
                    'Status' => 132,
                ]
            );

        //borramos los registros
        DB::table('visitas_campos')->where('visitas_campos.visitas_Id', '=', $request['id'])->delete();
        DB::table('visitas_todo')->where('visitas_todo.visitas_Id', '=', $request['id'])->delete();

        return response()->json(['message' => "ok"] );
    }
}
