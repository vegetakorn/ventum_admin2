<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class GettersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function cargaEmpresas(Request $request)
    {
        $sql = DB::table('empresas');
        $empresas = $sql->get();

        return response()->json(['empresas' => $empresas ] );
    }

    public function cargaRazones(Request $request)
    {
        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',$request['id']);
        $razones = $sql->get();
        $sql = DB::table('zonas');
        $sql->where('zonas.empresas_Id','=',$request['id']);
        $zonas = $sql->get();
        return response()->json(['razones' => $razones, 'zonas' => $zonas ] );
    }

    public function cargaPlazas(Request $request)
    {
        $sql = DB::table('plazas');
        $sql->where('plazas.razon_social_Id','=',$request['id']);
        $plazas = $sql->get();
        return response()->json(['plazas' => $plazas] );
    }

}
