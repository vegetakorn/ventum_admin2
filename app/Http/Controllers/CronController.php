<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CronExport;

class CronController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['crons']  =   DB::table('crons')
            ->leftjoin('empresas', function ($join) {
                $join->on('crons.empresas_Id', '=', 'empresas.Id');
            })
            ->select('crons.*', 'empresas.Nombre as Empresa' )->orderBy('crons.fhejecucion', 'desc')
            ->get();
        return view('cron/lista')->with($data);
    }

    public function export()
    {
        $data['crons'] =  DB::table('crons')
            ->leftjoin('empresas', function ($join) {
                $join->on('crons.empresas_Id', '=', 'empresas.Id');
            })
            ->select('crons.*', 'empresas.Nombre as Empresa' )
            ->where('crons.tipo_cron','=', 1)
            ->orderBy('crons.fhejecucion', 'desc')
            ->get();
        return Excel::download(new CronExport($data), 'crons.xlsx');
    }
}
