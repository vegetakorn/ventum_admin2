<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SucursalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $data_suc=    DB::table('tiendas')
            ->leftjoin('empresas', function ($join) {
                $join->on('tiendas.empresas_Id', '=', 'empresas.Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
            })
            ->select('tiendas.Id','tiendas.nombre as Tienda', 'tiendas.activo', 'tiendas.numsuc', 'tiendas.mail', 'empresas.Nombre as Empresa', 'razon_social.nombre as Razon', 'plazas.plaza' )
            ->where('empresas.Activo','=', 1)->get();

        $sucursales = collect($data_suc)->toArray();
        //buscamos la información financiera de cada tienda
        $arrSuc = array();

        for($k = 0;$k<count($sucursales);$k++)
        {
            //buscamos si la tienda tiene foda
            $foda =  DB::table('tiendas_foda')->where('tiendas_foda.tiendas_Id','=',$sucursales[$k]->Id)->count();

            $arrSuc[] = array(
                "Id" => $sucursales[$k]->Id,
                "Empresa" => $sucursales[$k]->Empresa,
                "Razon" => $sucursales[$k]->Razon,
                "Plaza" => $sucursales[$k]->plaza,
                "Clave" => $sucursales[$k]->numsuc,
                "Sucursal" => $sucursales[$k]->Tienda,
                "Mail" => $sucursales[$k]->mail,
                "Activo" => $sucursales[$k]->activo,
                "Foda" => $foda

            );
        }

        $data['tiendas'] = $arrSuc;
        return view('sucursales/lista')->with( $data);
    }

    public function add()
    {
        $data['empresas']  =   DB::table('empresas')->get();

        return view('sucursales/add')->with( $data);
    }

    public function insert(Request $request)
    {
        //buscamos si ya existe el correo electronico
        $sql = DB::table('tiendas');
        $sql->where('tiendas.mail','=',$request['email']);
        $tienda = $sql->count();
        if($tienda == 0)
        {
            //guardamos los campos
            DB::table('tiendas')->insert([
                [
                    'numsuc' => $request['clave'],
                    'nombre' => $request['nombre'],
                    'fhapertura' => $request['fecha'],
                    'direccion' => $request['direccion'],
                    'emp_ideal' => $request['emp_ideal'],
                    'telefono' => $request['telefono'],
                    'mail' => $request['email'],
                    'plaza_Id' => $request['plaza'],
                    'password' => $request['pass'],
                    'lon' => $request['long'],
                    'lat' => $request['lat'],
                    'empresas_Id' => $request['empresa'],
                    'razon_Id' => $request['razon'],
                    'zona' => $request['zona'],
                    'm2' => $request['m2'],
                    'dias_lim' => 0,
                    'activo' => 1

                ]
            ]);

            //id
            $id_suc = DB::getPdo()->lastInsertId();
            //insertamos foda
            DB::table('tiendas_foda')->insert([
                [
                    'tiendas_Id' => $id_suc,
                    'fortaleza' => "",
                    'oportunidad' => "",
                    'debilidad' => "",
                    'amenaza' => "",
                ]
            ]);

            //insertamos para que sea visible en la pagina de clientes, obteniendo el administrador de la empresa
            $sql = DB::table('users');
            $sql->where('users.empresas_Id','=',$request['empresa']);
            $sql->where('users.empleados_Id','=',0);
            $user = $sql->first();

            DB::table('users_tiendas')->insert([
                [
                    'tiendas_Id' => $id_suc,
                    'users_Id' => $user->id

                ]
            ]);
            return response()->json(['message' => $tienda] );
        }else
        {
            return response()->json(['message' => $tienda ] );
        }

    }


    public function foda(Request $request)
    {

        //guardamos los campos
        DB::table('tiendas_foda')->insert([
            [
                'fortaleza' => "",
                'oportunidad' => "",
                'debilidad' => "",
                'amenaza' => "",
                'tiendas_Id' => $request['id'],


            ]
        ]);
        return response()->json(['message' => "OK" ] );
    }

    public function edit($id)
    {
        $data['empresas']  =   DB::table('empresas')->get();

        $sql = DB::table('tiendas');
        $sql->where('tiendas.Id','=', $id);
        $data['tienda']  = $sql->first();

        return view('sucursales/edit')->with( $data);
    }
    public function update(Request $request)
    {
        //primero buscamos si el correo electronico ya existe en sistema, aparte del correo que tiene actualmente la tienda
        $sql = DB::table('tiendas');
        $sql->where('tiendas.Id','=', $request['id']);
        $tienda  = $sql->first();

        $sql = DB::table('tiendas');
        $sql->where('tiendas.mail','=', $request['email']);
        $sql->where('tiendas.Id','<>', $request['id']);
        $count  = $sql->count();
        if($count == 0)
        {
            DB::table('tiendas')
                ->where('tiendas.Id','=',$request['id'])
                ->update([
                        'numsuc' => $request['clave'],
                        'nombre' => $request['nombre'],
                        'fhapertura' => $request['fecha'],
                        'direccion' => $request['direccion'],
                        'emp_ideal' => $request['emp_ideal'],
                        'telefono' => $request['telefono'],
                        'mail' => $request['email'],
                        'plaza_Id' => $request['plaza'],
                        'password' => $request['pass'],
                        'lon' => $request['long'],
                        'lat' => $request['lat'],
                        'razon_Id' => $request['razon'],
                        'zona' => $request['zona'],
                        'm2' => $request['m2'],
                        'dias_lim' => 0
                    ]
                );

            //buscamos si la tienda esta asociada a un usuario
            $sql = DB::table('users');
            $sql->where('users.empleados_Id','=', $request['id'])
                ->where('users.tipo_user', "=", 2);
            $sucuser  = $sql->count();

            if($sucuser != 0)
            {
                DB::table('users')
                    ->where('users.empleados_Id','=',$request['id'])
                    ->update([

                            'email' => $request['email']
                        ]
                    );
            }

            return response()->json(['message' => $count ] );
        }else
        {
            return response()->json(['message' => $count] );
        }


    }

    public function activa(Request $request)
    {
        DB::table('tiendas')
            ->where('tiendas.Id','=',$request['id'])
            ->update([
                    'activo' => $request['flag']
                ]
            );
        return response()->json(['message' => "ok"] );

    }


}
