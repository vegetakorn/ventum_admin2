<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;



class EmpresasController extends Controller
{
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/ventum/public_html/uploads/'; //path para copia en clientes

    protected $rutaDirectorio = '/home/ventum/public_html/admin/uploads/Empresas';
    protected $rutaDirectorioHist = '/home/ventum/public_html/uploads/Empresas';
    protected $rutaDirectorioCron = '/home/ventum/laravel/public_html/Clientes/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $data['empresas'] =    DB::table('empresas')
            ->leftjoin('users', function ($join) {
                $join->on('users.empresas_Id', '=', 'empresas.Id');
            })
            ->select('empresas.*', 'users.name as super', 'users.email as supermail' )
            ->where('users.empleados_Id','=', 0)
            ->get();

        $sucursales = collect($data)->toArray();
        //buscamos la información financiera de cada tienda
        $arrSuc = array();

        for($k = 0;$k<count($sucursales);$k++)
        {

        }
        return view('empresas/lista')->with($data);
    }


    public function insert(Request $request)
    {


            //creamos la empresa
        DB::table('empresas')->insert([
            [
                'nombre' => $request['nombre'],
                'RFC' => $request['corto'],
                'Direccion' => $request['direccion'],
                'Telefono' => $request['tel'],
                'Web' => $request['web'],
                'Tiendas' => $request['notiendas'],
                'Logo' => $request['logo'],
                'Activo' => 1
            ]
        ]);
        $id_emp = DB::getPdo()->lastInsertId();
        $mailuser = $request['corto']."@ventum.com";

        //creamos carpeta para crons
        mkdir($this->rutaDirectorioCron.$request['corto'], 0755);
        //creamos carpetas de almacenamiento de archivos
        mkdir($this->rutaDirectorioCron.$request['corto']."/Historial", 0755);
        mkdir($this->rutaDirectorioCron.$request['corto']."/Respaldo", 0755);
        mkdir($this->rutaDirectorioCron.$request['corto']."/Ventas", 0755);
        mkdir($this->rutaDirectorioCron.$request['corto']."/Empleados", 0755);
        mkdir($this->rutaDirectorioCron.$request['corto']."/Todos", 0755);
        mkdir($this->rutaDirectorioCron.$request['corto']."/Presupuestos", 0755);

        //creamos el usuario
        DB::table('users')->insert([
            [
                'empresas_Id' =>  $id_emp,
                'empleados_Id' => 0,
                'name' =>  $request['nombre'],
                'email' =>  $mailuser,
                'password' =>  bcrypt('francia98'),
                'Activo' =>  1,
                'fotoEmp' => $request['logo'],
                'fotoUser' => $request['logo']
            ]
        ]);
        $id_usr = DB::getPdo()->lastInsertId();

        //creamos configuracion
        DB::table('configuracion')->insert([
            [
                'empresas_Id' =>  $id_emp,
                'valida_vis' => 0
            ]
        ]);
        //creamos configuracion de reportes
        DB::table('config_reportes')->insert([
            [
                'empresas_Id' =>  $id_emp,
                'reporte_visitas' => 1,
                'reporte_ventas' => 1
            ]
        ]);
        //alta de cuestionarios
        DB::table('cuestionario')->insert([['empresas_Id' =>  $id_emp, 'tipos_cuestionario_Id' => 1, 'Nombre' => 'Cuestionario de Visita']]);
        DB::table('cuestionario')->insert([['empresas_Id' =>  $id_emp, 'tipos_cuestionario_Id' => 2, 'Nombre' => 'Cuestionario de Seguimiento']]);
        DB::table('cuestionario')->insert([['empresas_Id' =>  $id_emp, 'tipos_cuestionario_Id' => 3, 'Nombre' => 'Baja Empleado']]);

        //agregamos familia, buscamos la configuracion básica de estrella de cuernavaca
        $fams =    DB::table('familias')->where('familias.empresas_Id','=', 1)->get();
        $familias = collect($fams)->toArray();
        for($k = 0;$k<count($familias);$k++)
        {
            //insertamos las familias
            DB::table('familias')->insert([
                [
                    'empresas_Id' =>  $id_emp,
                    'Nombre' => $familias[$k]->Nombre
                ]
            ]);
        }

        //agregamos razón social generica
        DB::table('razon_social')->insert([
            [
                'empresas_Id' =>  $id_emp,
                'Nombre' => $request['nombre']
            ]
        ]);
        $id_raz = DB::getPdo()->lastInsertId();

        //agregamos plaza generica
        DB::table('plazas')->insert([
            [
                'razon_social_Id' =>  $id_raz,
                'plaza' => $request['nombre']
            ]
        ]);

        //agregado de tiendas
        for($i = 0; $i < $request['notiendas']; $i++)
        {
            $nombre_suc = "sucursal_".$request['corto'].$i;
            $mail = "sucursal_".$request['corto'].$i."@".$request['corto'].".com";
            DB::table('tiendas')->insert([
                [
                    'empresas_Id' =>  $id_emp,
                    'nombre' => $nombre_suc,
                    'fhapertura' => date('Y-m-d'),
                    'mail' => $mail,
                    'activo' => 1,
                    'dias_lim' => 0
                ]
            ]);

            /** proceso completo de agregado de tiendas */
            $id_suc = DB::getPdo()->lastInsertId();
            //insertamos foda
            DB::table('tiendas_foda')->insert([
                [
                    'tiendas_Id' => $id_suc,
                    'fortaleza' => "",
                    'oportunidad' => "",
                    'debilidad' => "",
                    'amenaza' => "",
                ]
            ]);

            DB::table('users_tiendas')->insert([
                [
                    'tiendas_Id' => $id_suc,
                    'users_Id' => $id_usr
                ]
            ]);

        }

        //agregamos los titulos por defecto
        DB::table('titulos')->insert([
            [
                'empresas_Id' =>  $id_emp,
                'titulo_razon' => "Razón Social",
                'titulo_plaza' => "Plaza",
                'titulo_zona' => "Zona"
            ]
        ]);

        //agregamos los modulos, un for con 21 (se deberá aumentar conforme se agreguen nuevos modulos)
        for($j = 1;$j<=21; $j++)
        {
            DB::table('users_modulos')->insert([
                [
                    'users_Id' => $id_usr,
                    'modulos_Id' => $j
                ]
            ]);
        }

        //agregamos los widgets, un for con 9 (se deberá aumentar conforme se agreguen nuevos modulos)
        for($h = 1;$h<=9; $h++)
        {
            DB::table('users_widgets')->insert([
                [
                    'users_Id' => $id_usr,
                    'widgets_Id' => $h
                ]
            ]);
        }

        return response()->json(['message' => $request->all()] );


    }

    public function edit($id)
    {
        $data['empresa'] =    DB::table('empresas')
                                ->leftjoin('users', function ($join) {
                                    $join->on('users.empresas_Id', '=', 'empresas.Id');
                                })
                                ->select('empresas.*', 'users.name as super', 'users.email as supermail' )
                                ->where('empresas.Id','=', $id)
                                ->first();
        return view('empresas/edit')->with( $data);
    }

    public function update(Request $request)
    {
        DB::table('empresas')
            ->where('empresas.Id','=',$request['id'])
            ->update([
                    'Nombre' => $request['nombre'],
                    'Direccion' => $request['direccion'],
                    'Telefono' => $request['telefono'],
                    'Logo' => $request['logo'],
                    'Web' => $request['web']
                ]
            );

        DB::table('users')
            ->where('users.empresas_Id','=',$request['id'])
            ->update([
                    'fotoEmp' => $request['logo'],
                ]
            );
        return response()->json(['message' => "OK" ] );
    }

    public function activa(Request $request)
    {
        DB::table('empresas')
            ->where('empresas.Id','=',$request['id'])
            ->update([
                    'Activo' => $request['flag']
                ]
            );

        //desactivamos/activamos todos los usuarios correspondientes a la empresa

        DB::table('users')
            ->where('users.empresas_Id','=',$request['id'])
            ->update([
                    'Activo' => $request['flag']
                ]
            );


        return response()->json(['message' => "ok"] );

    }

    public function foto(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Empresa";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Empresas/', $nombre);
        $ruta = $this->path.'Empresas/'.$nombre;


        copy ($this->rutaDirectorio."/".$nombre,$this->rutaDirectorioHist."/".$nombre);
        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function add()
    {

        return view('empresas/add');
    }

}
