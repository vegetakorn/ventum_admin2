<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UsuariosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['usuarios']  =   DB::table('users')
            ->leftjoin('empresas', function ($join) {
                $join->on('users.empresas_Id', '=', 'empresas.Id');
            })
            ->select('users.*', 'empresas.Nombre as Empresa' )
            ->where('users.empleados_Id', "<>", 0)
            ->get();


        return view('usuarios/lista')->with( $data);
    }

    public function edit($id)
    {

        $sql = DB::table('users');
        $sql->where('users.id','=', $id);
        $data['user']  = $sql->first();

        return view('usuarios/edit')->with( $data);
    }

    public function update(Request $request)
    {
        //buscamos el tipo de usuario a cambiar el correo
        if($request['tipo'] == 1)
        {
            //primero buscamos si el correo electronico ya existe en sistema, aparte del correo que tiene actualmente los empleados
            $sql = DB::table('empleados');
            $sql->where('empleados.mail','=', $request['email']);
            $sql->where('empleados.Id','<>', $request['empleados_Id']);
            $count  = $sql->count();

        }else
        {
            //primero buscamos si el correo electronico ya existe en sistema, aparte del correo que tiene actualmente la tienda
            $sql = DB::table('tiendas');
            $sql->where('tiendas.mail','=', $request['email']);
            $sql->where('tiendas.Id','<>', $request['empleados_Id']);
            $count  = $sql->count();
        }

        if($count == 0)
        {
            DB::table('users')
                ->where('users.id','=',$request['id'])
                ->update([
                        'name' => $request['name'],
                        'email' => $request['email']
                    ]
                );

            if($request['tipo'] == 1)
            {
              //actualizamos el correo del empleado registrado
                DB::table('empleados')
                    ->where('empleados.Id','=',$request['empleados_Id'])
                    ->update([

                            'mail' => $request['email']
                        ]
                    );

            }else
            {
                //actualizamos el correo de la tienda
                DB::table('tiendas')
                    ->where('tiendas.Id','=',$request['empleados_Id'])
                    ->update([
                            'mail' => $request['email']
                        ]
                    );

            }
            return response()->json(['message' => $count ] );
        }else
        {
            return response()->json(['message' => $count] );
        }
    }

    public function activa(Request $request)
    {
        DB::table('users')
            ->where('users.Id','=',$request['id'])
            ->update([
                    'Activo' => $request['flag']
                ]
            );


        return response()->json(['message' => "ok"] );

    }
}
