<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte de Crons</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>

    <tr>
        <th >Empresa</th>
        <th >Fecha Subida:</th>
        <th >Hora:</th>
        <th >Nombre Archivo</th>
    </tr>
    </thead>
    <tbody>
    @foreach($info['crons'] as $cron )
        <tr>
            <td  >{{$cron->Empresa}}</td>
            <td  >{{date('d/m/Y', strtotime($cron->fhcreacion))}}</td>
            <td  >{{date('H:i:s', strtotime($cron->fhcreacion))}}</td>
            <td >{{$cron->archivo}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
