<!--left-fixed -navigation-->
<div class="sidebar" role="navigation">
    <div class="navbar-collapse">
        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right dev-page-sidebar mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar" id="cbp-spmenu-s1">
            <div class="scrollbar scrollbar1">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{route('dashboard.dash')}}" @yield('dashboard_active')><i class="fa fa-home nav_icon"></i>Inicio</a>
                    </li>
                    <li>
                        <a href="{{route('empresas.lista')}}" @yield('empresas_active')><i class="fa fa-building-o nav_icon"></i>Empresas</a>
                    </li>
                    <li>
                        <a href="{{route('sucursales.lista')}}" @yield('sucursales_active')><i class="fa fa-shopping-cart nav_icon"></i>Sucursales</a>
                    </li>
                    <li>
                        <a href="{{route('usuarios.lista')}}" @yield('usuarios_active')><i class="fa fa-users nav_icon"></i>Usuarios</a>
                    </li>
                    <li>
                        <a href="{{route('visitas.lista')}}" @yield('visitas_active')><i class="fa fa-archive nav_icon"></i>Visitas</a>
                    </li>
                    <li>
                        <a href="{{route('checklist.lista')}}" @yield('checklist_active')><i class="fa fa-check nav_icon"></i>Checklist</a>
                    </li>
                    <li>
                        <a href="{{route('todos.lista')}}" @yield('todos_active')><i class="fa fa-check-circle-o nav_icon"></i>To-Do's</a>
                    </li>
                    <li>
                        <a ><i class="fa fa-clock-o nav_icon"></i>Gestión de CRON <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a @yield('cron_active') href="{{route('cron.lista')}}">Bitácora</a>
                            </li>
                            <li>
                                <a >Ejecución Manual</a>
                            </li>
                        </ul>
                    </li>

                    <!--<li>
                        <a href="#"><i class="fa fa-cogs nav_icon"></i>Components <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="progressbar.html">Progressbar</a>
                            </li>
                            <li>
                                <a href="grid.html">Grid</a>
                            </li>
                        </ul>

                    </li>
                    <li>
                        <a href="#"><i class="fa fa-book nav_icon"></i>Elements <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="buttons.html">Buttons</a>
                            </li>
                            <li>
                                <a href="typography.html">Typography</a>
                            </li>
                        </ul>

                    </li>-->

                </ul>
            </div>
            <!-- //sidebar-collapse -->
        </nav>
    </div>
</div>
<!--left-fixed -navigation-->
