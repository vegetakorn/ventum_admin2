@section('empresas_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Listado de Empresas</h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div style="padding-bottom: 10px"  class="clearfix">
                            <a href="{{route('empresas.add')}}" class="btn btn_5 btn-lg btn-success warning_1 pull-right ">Agregar Empresa</a>
                        </div>
                        <table id="myTable" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>Logo</th>
                                <th>Empresa</th>
                                <th>Nombre Corto</th>
                                <th>Dirección</th>
                                <th>Teléfono</th>
                                <th>Web</th>
                                <th>Tiendas</th>

                                <th>Super Usuario</th>
                                <th>Activo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($empresas as $empresa)
                                <tr>
                                    <td> <img class="profile-user-img img-responsive" id="avatarImage" src="http://ventumsupervision.com/uploads/Empresas/{{$empresa->Logo}}" alt="User profile picture"></td>
                                    <td>{{$empresa->Nombre}}</td>
                                    <td>{{$empresa->RFC}}</td>
                                    <td>{{$empresa->Direccion}}</td>
                                    <td>{{$empresa->Telefono}}</td>
                                    <td>{{$empresa->Web}}</td>
                                    <td>{{$empresa->Tiendas}}</td>
                                    <td>{{$empresa->supermail}}</td>
                                    @if($empresa->Activo== 1)
                                        <td>Activo</td>
                                        <td>
                                            <a href="{{ route('empresas.edit', $empresa->Id) }}"  style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar"></a>
                                            <a onclick="flagActivacion(0, {{$empresa->Id}})" style="color: #9d100f" class="fa fa-times-circle" data-toggle="tooltip" data-placement="right" title="Baja"></a>
                                        </td>


                                    @else
                                        <td>Baja</td>
                                        <td>
                                            <a href="{{ route('empresas.edit',$empresa->Id) }}" style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar"></a>
                                            <a onclick="flagActivacion(1, {{$empresa->Id}})" style="color: #1f9d21" class="fa fa-check-circle-o" data-toggle="tooltip" data-placement="right" title="Activar"></a>

                                        </td>

                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--//grids-->

        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
@endsection
@section('js')
    <script language="javascript" type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ajax/editempresa.js')}}"></script>
    <script>
        var urlDel = '{{route('activaEmpresa')}}';
        var urlSuc = '{{route('empresas.lista')}}';
        var csrf = '{{csrf_token()}}'
        $(document).ready( function () {
            $('#myTable').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                }
            } );
        } );
    </script>
@endsection