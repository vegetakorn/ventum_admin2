@section('empresas_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Agregar Sucursal</h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div class="validation-grids widget-shadow" data-example-id="basic-forms">
                            <h4>Logo:</h4>
                            <br>
                            <form action="{{route('regFotoEmpresa')}}" method="post" style="visibility: hidden" id="avatarForm">
                                  {{ csrf_field() }}
                                <input style="height: 20%; width: 20%" type="file" id="avatarInput" name="photo">
                                <div class="foto_perfil" >
                                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                    <input type="hidden" id="foto_perfil" value="noimage2.png">
                                </div>

                            </form>
                            <div class="rutaFoto">
                                <img onclick="setFoto()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="http://ventumsupervision.com/admin/uploads/Empresas/noimage2.png" alt="User profile picture">

                            </div>
                            <form data-toggle="validator" novalidate="true" enctype="multipart/form-data">
                                <h4>Nombre:</h4>
                                <br>
                                <div class="form-group valid-form">
                                    <input type="text" class="form-control" id="inputNombre" placeholder="Nombre" required="">
                                </div>
                                <h4>Nombre Corto (no poner espacios):</h4>
                                <br>
                                <div class="form-group valid-form">
                                    <input type="text" class="form-control" id="inputCorto" placeholder="Nombre corto" required="">
                                </div>
                                <h4>Tiendas a Agregar</h4>
                                <br>
                                <div class="form-group valid-form">
                                    <input type="number" class="form-control" id="inputSuc" placeholder="0" required="">
                                </div>
                                <h4>Dirección:</h4>
                                <br>
                                <div class="form-group valid-form">
                                    <input type="text" class="form-control" id="inputDir" placeholder="Direccion" required="">
                                </div>
                                <h4>Teléfono:</h4>
                                <br>
                                <div class="form-group valid-form">
                                    <input type="text" class="form-control" id="inputTel" placeholder="Telefono" required="">
                                </div>
                                <h4>Página Web</h4>
                                <br>
                                <div class="form-group valid-form">
                                    <input type="text" class="form-control" id="inputWeb" placeholder="Página" required="">
                                </div>
                                <div class="form-group">
                                    <a onclick="registraEmpresa()" class="btn btn-primary ">Agregar</a>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!--//grids-->

        </div>
    </div>


@endsection
@section('css')

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@endsection
@section('js')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    <script>

        var urlAdd = '{{route('insertEmpresa')}}';
        var urlEmp = '{{route('empresas.lista')}}';
        var csrf = '{{csrf_token()}}'
        $(document).ready( function () {


        } );
    </script>
    <script src="{{ asset('ajax/addempresa.js')}}"></script>


@endsection
