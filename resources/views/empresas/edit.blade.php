@section('empresas_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Editar Empresa {{$empresa->Nombre}} </h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div class="validation-grids widget-shadow" data-example-id="basic-forms">
                            <div class="form-body form-body-info">
                                <form action="{{route('regFotoEmpresa')}}" method="post" style="visibility: hidden" id="avatarForm">
                                    {{ csrf_field() }}
                                    <input style="height: 20%; width: 20%" type="file" id="avatarInput" name="photo">
                                    <div class="foto_perfil" >
                                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                        <input type="hidden" id="foto_perfil" value="noimage2.png">
                                    </div>

                                </form>
                                <div class="rutaFoto">
                                    <img onclick="setFotoEdit()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="http://ventumsupervision.com/admin/uploads/Empresas/noimage2.png" alt="User profile picture">

                                </div>
                                <form data-toggle="validator" novalidate="true">
                                    <h4>Nombre:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$empresa->Nombre}}" class="form-control" id="inputNombre" placeholder="Nombre" required="">
                                    </div>
                                    <h4>Nombre Corto:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$empresa->RFC}}" class="form-control" id="inputShort" disabled placeholder="Nombre Corto" required="">
                                    </div>
                                    <h4>Dirección</h4>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$empresa->Direccion}}" class="form-control" id="inputDireccion" placeholder="Direccion" required="">
                                    </div>
                                    <h4>Teléfono:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$empresa->Telefono}}" class="form-control" id="inputTelefono" placeholder="Teléfono" required="">
                                    </div>
                                    <h4>Página Web:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$empresa->Web}}" class="form-control" id="inputWeb" placeholder="Teléfono" required="">
                                    </div>
                                    <h4>Cuenta Superadmin</h4>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$empresa->supermail}}" class="form-control" id="inputMail" disabled placeholder="Direccion" required="">
                                    </div>
                                    <div class="form-group">
                                        <a onclick="updateEmpresa()" class="btn btn-primary ">Actualizar</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--//grids-->
        </div>
    </div>

@endsection
@section('css')
    <style>
        .modal-backdrop {
            /* bug fix - no overlay */
            display: none;
        }
    </style>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@endsection
@section('js')
    <script src="{{ asset('ajax/editempresa.js')}}"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        var urlUpd = '{{route('updateEmpresa')}}';
        var urlSuc = '{{route('empresas.lista')}}';
        var csrf = '{{csrf_token()}}'
        var id = '{{$empresa->Id}}'
        $(document).ready( function () {

        } );
    </script>
@endsection
