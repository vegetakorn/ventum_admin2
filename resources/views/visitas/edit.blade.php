@section('visitas_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Editar Visita </h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div class="validation-grids widget-shadow" data-example-id="basic-forms">
                            <div class="form-body form-body-info">
                                <form data-toggle="validator" novalidate="true">
                                    <h4>Empresa</h4>
                                    <br>
                                    <div class="form-group"> <!-- Date input -->
                                        <input type="text" value="{{$visita->Empresa}}"  disabled class="form-control" id="inputNA" required="">

                                    </div>
                                    <h4>Tienda</h4>
                                    <br>
                                    <div class="form-group"> <!-- Date input -->
                                        <input type="text" value="{{$visita->Tienda}}"  disabled class="form-control" id="inputNA"  required="">

                                    </div>
                                    <h4>Checklist</h4>
                                    <br>
                                    <div class="form-group"> <!-- Date input -->
                                        <input type="text" value="{{$visita->Checklist}}"  disabled class="form-control" id="inputNA"  required="">

                                    </div>
                                    <h4>Fecha de Inicio:</h4>
                                    <br>
                                    <div class="form-group"> <!-- Date input -->
                                        <input type="text" value="{{$visita->HoraInicio}}" class="form-control" id="inputIni" placeholder="{{$visita->HoraInicio}}" required="">
                                        <span class="help-block with-errors">Respeta la estructura de la cadena, editar con mucha atención</span>
                                    </div>
                                    <h4>Fecha de Finalización:</h4>
                                    <br>
                                    <div class="form-group"> <!-- Date input -->
                                        <input type="text" value="{{$visita->HoraFin}}" class="form-control" id="inputFin" placeholder="{{$visita->HoraFin}}" required="">
                                        <span class="help-block with-errors">Respeta la estructura de la cadena, editar con mucha atención</span>
                                    </div>

                                    <h4>Duración:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="number" value="" class="form-control" id="inputDuracion" placeholder="{{$visita->Duracion}}" required="">
                                        <span class="help-block with-errors">Solo puedes definir la nueva duración en minutos</span>
                                    </div>
                                    <div class="form-group">
                                        <a onclick="updateVisita()" class="btn btn-primary ">Actualizar</a>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!--//grids-->

        </div>
    </div>
@endsection
@section('css')
    <style>
        .modal-backdrop {
            /* bug fix - no overlay */
            display: none;
        }
    </style>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@endsection
@section('js')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('ajax/editvisitas.js')}}"></script>
    <script>
        var urlEdit = '{{route('updateVisita')}}'
        var urlVis = '{{route('visitas.lista')}}'
        var csrf = '{{csrf_token()}}';
        var id = '{{$visita->Id}}'
        var dur = '{{$visita->Duracion}}'
        $(document).ready( function () {
            $('.my-select').selectpicker();
            var date_input=$('input[name="date"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        } );
    </script>
@endsection
