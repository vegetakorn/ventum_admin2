@section('visitas_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Listado de  Visitas</h2>
                    <span style="color:#e21110;" class="help-block with-errors">INFORMACIÓN DE TRATO DELICADO, USAR CON MUCHA ATENCIÓN, RIESGO DE PÉRDIDA IRRECUPERABLE DE DATOS</span>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <table id="myTable" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>Empresa</th>
                                <th>Clave</th>
                                <th>Tienda</th>
                                <th>Supervisor</th>
                                <th>Checklist</th>
                                <th>Fecha</th>
                                <th>Duración</th>
                                <th>Calificacion</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visitas as $visita)
                                <tr>
                                    <td>{{$visita->Empresa}}</td>
                                    <td>{{$visita->numsuc}}</td>
                                    <td>{{$visita->Tienda}}</td>
                                    <td>{{$visita->Usuario}}</td>
                                    @if($visita->Checklist == "")
                                        <td>Seguimiento</td>
                                    @else
                                        <td>{{$visita->Checklist}}</td>
                                    @endif
                                    <td>{{$visita->HoraInicio}}</td>
                                    <td>{{$visita->Duracion}}</td>
                                    <td>{{$visita->Calif}}</td>
                                    <td>
                                        <a href="{{ route('visitas.edit', $visita->Id) }}"  style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar Fecha"></a>
                                        <a  onclick="flagActivacion({{$visita->Id}})" style="color: #9d100f" class="fa fa-times-circle" data-toggle="tooltip" data-placement="right" title="Cancelar Visita"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--//grids-->
        </div>
    </div>
@endsection
@section('css')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
@endsection
@section('js')
    <!-- Latest compiled and minified JavaScript -->
    <script src="{{ asset('ajax/editvisitas.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script language="javascript" type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        var csrf = '{{csrf_token()}}'
        var urlDel = '{{route('cancelaVisita')}}'
        var urlVis = '{{route('visitas.lista')}}'
        $(document).ready( function () {

            $('#myTable').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                }
            } );
            /* $.ajax({
                 method: 'POST',
                 url: urlEmp,
                 data: {id: 0, _token: csrf  }
             })
                 .done(function(msg){
                     // console.log(msg['razones']);
                     for(var i = 0; i < msg['empresas'].length; i++)
                     {
                         $("#selectEmp").append('<option value="'+msg['empresas'][i]['Id']+'" >'+msg['empresas'][i]['Nombre']+'</option>');
                     }
                     $("#selectEmp").val(0);
                     $("#selectEmp").selectpicker("refresh");

                 });*/

        } );
    </script>
@endsection
