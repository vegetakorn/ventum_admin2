@section('cron_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Bitácora de ejecución de CRONS</h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div class="validation-grids widget-shadow" data-example-id="basic-forms">

                            <div class="form-body form-body-info">
                                <form data-toggle="validator" novalidate="true">

                                    <h4>Selecciona una Empresa:</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select     class="selectpicker form-control" data-header="Busca una empresa" id="selectEmp" data-live-search="true">
                                                    <option value="0" selected>Selecciona</option>

                                                </select>
                                            </div>

                                        </div>

                                    </div>
                                </form>
                                <div class="form-group">
                                    <a class="btn btn-primary ">Ver</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
          //grids-->
            <!--grids-->
            <div class="grids">
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div style="padding-bottom: 10px"  class="clearfix">
                            <a href="{{route('export')}}" class="btn btn_5 btn-lg btn-success warning_1 pull-right ">Reporte FTP</a>
                        </div>
                        <table id="myTable" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>Empresa</th>
                                <th>Tipo</th>
                                <th>Archivo</th>
                                <th>Fecha Subida FTP</th>
                                <th>Fecha ejecución de CRON</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($crons as $cron)
                                <tr>
                                    <td>{{$cron->Empresa}}</td>
                                    @switch($cron->tipo_cron)
                                        @case(1)
                                        <td>CRON de registro de ventas</td>
                                        @break
                                        @case(2)
                                        <td>CRON de actualización de ventas</td>
                                        @break
                                        @case(3)
                                        <td>CRON de respaldo de ventas</td>
                                        @break
                                        @case(4)
                                        <td>CRON de correos a supervisores</td>
                                        @break
                                    @endswitch
                                    <td>{{$cron->archivo}}</td>
                                    <td>{{$cron->fhcreacion}}</td>
                                    <td>{{$cron->fhejecucion}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!--//grids-->

        </div>
    </div>
@endsection
@section('css')

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
@endsection
@section('js')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script language="javascript" type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        var urlEmp = '{{route('cargaEmpresas')}}'
        var csrf = '{{csrf_token()}}'
        $(document).ready( function () {

            $('#myTable').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                }
            } );

           /* $.ajax({
                method: 'POST',
                url: urlEmp,
                data: {id: 0, _token: csrf  }
            })
                .done(function(msg){
                    // console.log(msg['razones']);
                    for(var i = 0; i < msg['empresas'].length; i++)
                    {
                        $("#selectEmp").append('<option value="'+msg['empresas'][i]['Id']+'" >'+msg['empresas'][i]['Nombre']+'</option>');
                    }
                    $("#selectEmp").val(0);
                    $("#selectEmp").selectpicker("refresh");

                });*/

        } );
    </script>

@endsection
