<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>Ventum Administración</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Baxster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="admin/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="admin/css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="icon" href="favicon.ico" type="image/x-icon" >
    <!-- font-awesome icons -->
    <link href="admin/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!-- js -->
    <script src="admin/js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
</head>
<body class="login-bg" style="background-color: #6b9dbb">
<div class="login-body" >
    <div class="login-heading" style="position: center">
        <img class="centerimg" src="admin/images/logo_ventum.png">
    </div>
    <div class="login-info">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <input id="email" type="text" class="user" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
            <input id="password" type="password" class="lock" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
            <div class="forgot-top-grids">
                <div class="forgot-grid">
                    <ul>
                        <li>
                            <input type="checkbox" id="brand1" value="">
                            <!--<label for="brand1"><span></span>Remember me</label>-->
                        </li>
                    </ul>
                </div>
                <div class="forgot">
                    <!-- <a href="#">Forgot password?</a>-->

                </div>
                <div class="clearfix"> </div>
            </div>
            <input type="submit" name="Sign In" value="Entrar">
        </form>
    </div>
</div>
<div class="go-back login-go-back" >

</div>
<div class="go-back login-go-back">

</div>
<div class="copyright login-copyright" >

</div>
</body>
</html>