@section('sucursales_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Editar Sucursal {{$tienda->nombre}} </h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div class="validation-grids widget-shadow" data-example-id="basic-forms">

                            <div class="form-body form-body-info">
                                <form data-toggle="validator" novalidate="true">

                                    <h4>Empresa:</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select   disabled  class="selectpicker form-control" data-header="Busca una empresa" id="selectEmp" data-live-search="true">
                                                    <option value="0" selected>Selecciona</option>
                                                    @foreach($empresas as $empresa)
                                                        <option value="{{$empresa->Id}}">{{$empresa->Nombre}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Razón:</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">

                                                <select class="selectpicker form-control" id="selectRaz"  name="selectRaz" data-live-search="true">
                                                    <option value="0" selected>Selecciona</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Plaza:</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" id="selectPla" data-live-search="true">
                                                    <option value="0">Selecciona</option>
                                                    <div class="plaza">

                                                    </div>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Zona:</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" id="selectZon" data-live-search="true">
                                                    <option value="0">Selecciona</option>
                                                    <div class="zona">

                                                    </div>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Clave:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->numsuc}}" class="form-control" id="inputClave" placeholder="Clave" required="">
                                    </div>
                                    <h4>Nombre:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->nombre}}" class="form-control" id="inputName" placeholder="Nombre de Sucursal" required="">
                                    </div>
                                    <h4>Fecha de Ingreso:</h4>
                                    <br>
                                    <div class="form-group"> <!-- Date input -->
                                        <input class="form-control" value="{{$tienda->fhapertura}}" id="inputDate" name="date" placeholder="YYYY-MM-DD"  type="text"/>
                                    </div>
                                    <h4>Correo Electrónico</h4>
                                    <br>
                                    <div class="form-group has-feedback">
                                        <input type="email" class="form-control" value="{{$tienda->mail}}" id="inputEmail" placeholder="Email" data-error="Correo inválido" required="">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <span class="help-block with-errors">Introduce un correo electrónico válido</span>
                                    </div>
                                    <h4>Direccion:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->direccion}}" class="form-control" id="inputDireccion" placeholder="Direccion" required="">
                                    </div>
                                    <h4>Empleados:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="number" value="{{$tienda->emp_ideal}}" class="form-control" id="inputEmp" placeholder="Empleados Ideal" required="">
                                    </div>
                                    <h4>Metros Cuadrados:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->m2}}" class="form-control" id="inputM2" placeholder="Metros Cuadrados" required="">
                                    </div>
                                    <h4>Teléfono:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->telefono}}" class="form-control" id="inputTelefono" placeholder="Teléfono" required="">
                                    </div>
                                    <h4>Contraseña:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->password}}" class="form-control" id="inputPass" placeholder="Contraseña" required="">
                                    </div>

                                    <h4>Latitud:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->lat}}" class="form-control" id="inputLat" placeholder="Latitud" required="">
                                    </div>
                                    <h4>Longitud:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$tienda->lon}}" class="form-control" id="inputLong" placeholder="Longitud" required="">
                                    </div>
                                    <div class="form-group">
                                        <a onclick="updateTienda()" class="btn btn-primary ">Actualizar</a>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!--//grids-->

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="alertModal" style="position: absolute" tabindex="-1"  role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar Sucursal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Debes seleccionar una empresa
                </div>
                <div class="modal-footer">
                    <button  type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .modal-backdrop {
            /* bug fix - no overlay */
            display: none;
        }
    </style>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@endsection
@section('js')
    <script src="{{ asset('ajax/editsucursales.js')}}"></script>
    <script src="{{ asset('ajax/addsucursales.js')}}"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        var urlRaz = '{{route('cargaRazones')}}';
        var urlPla = '{{route('cargaPlazas')}}';
        var urlUpd = '{{route('updateTienda')}}';
        var urlSuc = '{{route('sucursales.lista')}}';
        var csrf = '{{csrf_token()}}'
        var emp = '{{$tienda->empresas_Id}}';
        var raz = '{{$tienda->razon_Id}}';
        var pla = '{{$tienda->plaza_Id}}';
        var zon = '{{$tienda->zona}}';
        var id = '{{$tienda->Id}}'
        $(document).ready( function () {
            $('.my-select').selectpicker();
            var date_input=$('input[name="date"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
            $("#selectEmp").val(emp);
            setRazonZona();
        } );
    </script>
@endsection
