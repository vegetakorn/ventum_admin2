@section('sucursales_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Listado de  Sucursales</h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div style="padding-bottom: 10px"  class="clearfix">
                            <a href="{{route('sucursales.add')}}" class="btn btn_5 btn-lg btn-success warning_1 pull-right ">Agregar Tienda</a>
                        </div>
                        <table id="myTable" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Empresa</th>
                                    <th>Razón</th>
                                    <th>Plaza</th>
                                    <th>Clave</th>
                                    <th>Sucursal</th>
                                    <th>E-Mail</th>
                                    <th>FODA</th>
                                    <th>Status</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($tiendas as $tienda)
                                <tr>
                                    <td>{{$tienda['Empresa']}}</td>
                                    <td>{{$tienda['Razon']}}</td>
                                    <td>{{$tienda['Plaza']}}</td>
                                    <td>{{$tienda['Clave']}}</td>
                                    <td>{{$tienda['Sucursal']}}</td>
                                    <td>{{$tienda['Mail']}}</td>
                                    @if($tienda['Foda'] == 0)
                                        <td>NO</td>
                                    @else
                                        <td>SI</td>
                                    @endif
                                    @if($tienda['Activo'] == 1)
                                        <td>Activo</td>
                                        <td>
                                            <a href="{{ route('sucursales.edit', $tienda['Id']) }}"  style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar"></a>
                                            <a onclick="flagActivacion(0, {{$tienda['Id']}})" style="color: #9d100f" class="fa fa-times-circle" data-toggle="tooltip" data-placement="right" title="Baja"></a>

                                            @if($tienda['Foda'] == 0)
                                                <a onclick="validaFODA( {{$tienda['Id']}})" style="color: #f9b14a" class="fa fa-archive" data-toggle="tooltip" data-placement="right" title="Validar FODA"></a>
                                            @endif
                                        </td>
                                    @else
                                        <td>Baja</td>
                                        <td>
                                            <a href="{{ route('sucursales.edit',$tienda['Id']) }}" style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar"></a>
                                            <a onclick="flagActivacion(1, {{$tienda['Id']}})" style="color: #1f9d21" class="fa fa-check-circle-o" data-toggle="tooltip" data-placement="right" title="Activar"></a>

                                            @if($tienda['Foda'] == 0)
                                                <a onclick="validaFODA( {{$tienda['Id']}})" style="color: #f9b14a" class="fa fa-archive" data-toggle="tooltip" data-placement="right" title="Validar FODA"></a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--//grids-->
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
@endsection
@section('js')
    <script language="javascript" type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ajax/editsucursales.js')}}"></script>
    <script>
        var urlDel = '{{route('activaTienda')}}';
        var urlFoda = '{{route('asignaFoda')}}';
        var urlSuc = '{{route('sucursales.lista')}}';
        var csrf = '{{csrf_token()}}'
        $(document).ready( function () {
            $('#myTable').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                }
            } );
        } );
    </script>
@endsection