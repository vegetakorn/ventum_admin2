@section('usuarios_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Listado de  Usuarios</h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <table id="myTable" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th>Empresa</th>
                                <th>Usuario</th>
                                <th>Mail</th>
                                <th>Tipo</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{$usuario->Empresa}}</td>
                                    <td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>
                                    @if($usuario->tipo_user == 1)
                                        <td>Empleado</td>

                                    @else
                                        <td>Tienda</td>
                                    @endif
                                    @if($usuario->Activo == 1)
                                        <td>Activo</td>
                                        <td>
                                            <a href="{{ route('usuarios.edit', $usuario->id) }}"  style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar"></a>
                                            <a onclick="flagActivacion(0, {{$usuario->id, $usuario->tipo_user, $usuario->empleados_Id}} )" style="color: #9d100f" class="fa fa-times-circle" data-toggle="tooltip" data-placement="right" title="Baja"></a>

                                        </td>
                                    @else
                                        <td>Baja</td>
                                        <td>
                                            <a href="{{ route('usuarios.edit', $usuario->id) }}" style="color: #24379d" class="fa fa-edit" data-toggle="tooltip" data-placement="right" title="Editar"></a>
                                            <a onclick="flagActivacion(1, {{$usuario->id, $usuario->tipo_user, $usuario->empleados_Id}} )" style="color: #1f9d21" class="fa fa-check-circle-o" data-toggle="tooltip" data-placement="right" title="Activar"></a>

                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--//grids-->

        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
@endsection
@section('js')
    <script language="javascript" type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ajax/editusuarios.js')}}"></script>
    <script>
        var urlAct = '{{route('activaUser')}}';
        var urlUser = '{{route('usuarios.lista')}}';
        var csrf = '{{csrf_token()}}'

        $(document).ready( function () {
            $('#myTable').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                }
            } );
        } );
    </script>
@endsection