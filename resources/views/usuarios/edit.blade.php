@section('usuarios_active')
    class="active"
@endsection
@extends('welcome')
@section('body')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!--grids-->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Editar Usuario {{$user->name}} </h2>
                </div>
                <div class="panel panel-widget">
                    <div class="block-page">
                        <div class="validation-grids widget-shadow" data-example-id="basic-forms">

                            <div class="form-body form-body-info">
                                <form data-toggle="validator" novalidate="true">

                                    <h4>Nombre:</h4>
                                    <br>
                                    <div class="form-group valid-form">
                                        <input type="text" value="{{$user->name}}" class="form-control" id="inputName" placeholder="Clave" required="">
                                    </div>

                                    <h4>Correo Electrónico</h4>
                                    <br>
                                    <div class="form-group has-feedback">
                                        <input type="email" class="form-control" value="{{$user->email}}" id="inputEmail" placeholder="Email" data-error="Correo inválido" required="">
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <span class="help-block with-errors">Introduce un correo electrónico válido</span>
                                    </div>

                                    <div class="form-group">
                                        <a onclick="updateUser()" class="btn btn-primary ">Actualizar</a>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!--//grids-->

        </div>
    </div>
@endsection
@section('css')

@endsection
@section('js')
    <script src="{{ asset('ajax/editusuarios.js')}}"></script>
    <script>
        var urlUpd = '{{route('updateUsuario')}}';
        var urlUser = '{{route('usuarios.lista')}}';
        var csrf = '{{csrf_token()}}'
        var id = '{{$user->id}}'
        var emp_Id = '{{$user->empleados_Id}}'
        var tipo = '{{$user->tipo_user}}'
        $(document).ready( function () {
        } );
    </script>
@endsection
