
<!DOCTYPE HTML>
<html>
<head>
    <title>Ventum Administración</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Baxster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <style>
        .modal-backdrop {
            /* bug fix - no overlay */
            display: none;
        }
    </style>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('admin/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="{{ asset('admin/css/style.css')}}" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="icon" href="favicon.ico" type="image/x-icon" >
    <!-- font-awesome icons -->
    <link href="{{ asset('admin/css/font-awesome.css')}}" rel="stylesheet">

    <!-- //font-awesome icons -->
    <!-- chart -->
    <script src="{{ asset('admin/js/Chart.js')}}"></script>
    <!-- //chart -->
    <!-- js-->
    <script src="{{ asset('admin/js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{ asset('admin/js/modernizr.custom.js')}}"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="{{ asset('admin/css/animate.css')}}" rel="stylesheet" type="text/css" media="all">

    <script src="{{ asset('admin/js/wow.min.js')}}"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="{{ asset('admin/js/metisMenu.min.js')}}"></script>
    <script src="{{ asset('admin/js/custom.js')}}"></script>
    <link href="{{ asset('admin/css/custom.css')}}" rel="stylesheet">
    <!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content">

    @include('layouts.sidebar')
    @include('layouts.header')
    @yield('body')
    @include('layouts.footer')
</div>
<!-- Classie -->
<script src="{{ asset('admin/js/classie.js')}}"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

        showLeftPush.onclick = function() {
            classie.toggle( this, 'active' );
            classie.toggle( body, 'cbp-spmenu-push-toright' );
            classie.toggle( menuLeft, 'cbp-spmenu-open' );
            disableOther( 'showLeftPush' );
        };

    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!-- Bootstrap Core JavaScript -->

<script type="text/javascript" src="{{ asset('admin/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/dev-loaders.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/dev-layout-default.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery.marquee.js')}}"></script>
<link href="{{ asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">

<!-- candlestick -->
<script type="text/javascript" src="{{ asset('admin/js/jquery.jqcandlestick.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/jqcandlestick.css')}}" />
<!-- //candlestick -->

<!--max-plugin-->
<script type="text/javascript" src="{{ asset('admin/js/plugins.js')}}"></script>
<!--//max-plugin-->

<!--scrolling js-->
<script src="{{ asset('admin/js/jquery.nicescroll.js')}}"></script>
<script src="{{ asset('alert/bootbox.min.js') }}" ></script>
<script src="{{ asset('admin/js/scripts.js')}}"></script>
<!--//scrolling js-->

<!-- real-time-updates -->
<script language="javascript" type="text/javascript" src="{{ asset('admin/js/jquery.flot.js')}}"></script>
@yield('css')
@yield('js')
</body>
</html>