<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//servicios de home
Route::get('/dashboard/dash', 'HomeController@dash')->name('dashboard.dash');

//servicios de empresas
Route::get('/empresas/lista', 'EmpresasController@index')->name('empresas.lista');
Route::get('/empresas/add', 'EmpresasController@add')->name('empresas.add');
Route::get('/empresas/edit/{id}', 'EmpresasController@edit')->name('empresas.edit')->where('id', '[0-9]+');
Route::post('activaEmpresa', 'EmpresasController@activa')->name('activaEmpresa');
Route::post('updateEmpresa', 'EmpresasController@update')->name('updateEmpresa');
Route::post('insertEmpresa', 'EmpresasController@insert')->name('insertEmpresa');
Route::post('/regFotoEmpresa', 'EmpresasController@foto')->name('regFotoEmpresa');

//servicios de sucursales
Route::get('/sucursales/lista', 'SucursalesController@index')->name('sucursales.lista');
Route::get('/sucursales/add', 'SucursalesController@add')->name('sucursales.add');
Route::post('insertTienda', 'SucursalesController@insert')->name('insertTienda');
Route::post('updateTienda', 'SucursalesController@update')->name('updateTienda');
Route::post('activaTienda', 'SucursalesController@activa')->name('activaTienda');
Route::post('asignaFoda', 'SucursalesController@foda')->name('asignaFoda');
Route::get('/sucursales/edit/{id}', 'SucursalesController@edit')->name('sucursales.edit')->where('id', '[0-9]+');


//servicios de usuarios
Route::get('/usuarios/lista', 'UsuariosController@index')->name('usuarios.lista');
Route::get('/usuarios/edit/{id}', 'UsuariosController@edit')->name('usuarios.edit')->where('id', '[0-9]+');
Route::post('updateUsuario', 'UsuariosController@update')->name('updateUsuario');
Route::post('activaUser', 'UsuariosController@activa')->name('activaUser');

//servicios de visitas
Route::get('/visitas/lista', 'VisitasController@index')->name('visitas.lista');
Route::get('/visitas/edit/{id}', 'VisitasController@edit')->name('visitas.edit')->where('id', '[0-9]+');
Route::post('updateVisita', 'VisitasController@update')->name('updateVisita');
Route::post('cancelaVisita', 'VisitasController@cancel')->name('cancelaVisita');

//servicios de todos
Route::get('/todos/lista', 'TodosController@index')->name('todos.lista');

//servicios de checklist
Route::get('/checklist/lista', 'ChecklistController@index')->name('checklist.lista');

//servicios de cron
Route::get('/cron/lista', 'CronController@index')->name('cron.lista');
Route::get('export', 'CronController@export')->name('export');


//getters
Route::post('cargaRazones', 'GettersController@cargaRazones')->name('cargaRazones');
Route::post('cargaPlazas', 'GettersController@cargaPlazas')->name('cargaPlazas');
Route::post('cargaEmpresas', 'GettersController@cargaEmpresas')->name('cargaEmpresas');

//subida de archivos
Route::get('formulario', 'StorageController@index');


Route::get('/logout', function()
{
    Auth::logout();
    Session::flush();
    return Redirect::to('/');
})->name('logout');


