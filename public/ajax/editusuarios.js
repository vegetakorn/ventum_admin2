function updateUser()
{

    var nombre = document.getElementById('inputName').value;
    var email = document.getElementById('inputEmail').value;


        if(nombre != "")
        {
            if(email != "")
            {
                $.ajax({
                    method: 'POST',
                    url: urlUpd,
                    data: {
                        name:  nombre,
                        email:  email,
                        empleados_Id: emp_Id,
                        tipo: tipo,
                        id: id,
                        _token: csrf
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        if(msg['message'] != 0)
                        {
                            bootbox.alert("El correo electrónico ya existe en sistema, favor de verificar");
                        }else
                        {
                            window.location.replace(urlUser);
                        }
                    });
            }else
            {
                bootbox.alert("Debes escribir un Correo Electrónico");
            }
        }else
        {
            bootbox.alert("Debes definir un nombre");
        }

}

function flagActivacion(flag, id, tipo, emp)
{
    var activar = flag;
    var user_id = id;
    var tipou = tipo;
    var text = "";
    var empId = emp;
    if(flag == 0)
    {
        text = "desactivar";
    }else
    {
        text = "activar";
    }

    /** **/


    bootbox.confirm({
        message: "¿Realmente deseas " + text + " este usuario?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true)
            {
                $.ajax({
                    method: 'POST',
                    url: urlAct,
                    data: {id: user_id, tipo:tipou, empleados_Id: empId, flag: activar, _token: csrf  }
                })
                    .done(function(msg){
                        // console.log(msg['razones']);
                        window.location.replace(urlUser);

                    });
            }else
            {

            }
        }
    });




}