function setFoto()
{
    $avatarImage = $('#avatarImage');
    $avatarInput = $('#avatarInput');
    $avatarForm = $('#avatarForm');
    $rutaFoto = $('.rutaFoto');
    $avatarInput.click();
    $avatarInput.on('change', function () {
        var formData = new FormData();
        formData.append('photo', $avatarInput[0].files[0]);
        $('#modal-info').modal('toggle');

        $.ajax({
            url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
            method: $avatarForm.attr('method'),
            data: formData,
            processData: false,
            contentType: false
        }).done(function (msg) {
            // alert(cam);
            if (data.success)
                $avatarImage.attr('src', data.path);
              var URLdomain = window.location.host;
              var pathImg = 'http://'+URLdomain+'/'+msg['path'];
              $avatarImage.attr('src', pathImg)
               console.log(msg['path']);
           // $('#modal-info').modal('toggle');

            $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['message'] + '" >')


        });
    });
}

function registraEmpresa()
{


    var logo = document.getElementById('foto_perfil').value;
    var nombre = document.getElementById('inputNombre').value;
    var corto = document.getElementById('inputCorto').value;
    var notiendas = document.getElementById('inputSuc').value;
    var direccion = document.getElementById('inputDir').value;
    var tel = document.getElementById('inputTel').value;
    var web = document.getElementById('inputWeb').value;


    if(nombre != "")
    {
        if(corto != "")
        {

                bootbox.alert("Agregando empresa, espera un momento");
                $.ajax({
                    method: 'POST',
                    url: urlAdd,
                    data: {
                        logo:  logo,
                        nombre:  nombre,
                        corto:  corto,
                        notiendas:  notiendas,
                        direccion:  direccion,
                        tel:  tel,
                        web:  web,
                        _token: csrf
                    }
                })
                    .done(function(msg){
                            console.log(msg['message']);

                            window.location.replace(urlEmp);


                    });



        }else
        {
            bootbox.alert("Debes definir un nombre corto");
        }
    }else
    {
        bootbox.alert("Debes elegir un nombre");
    }


}