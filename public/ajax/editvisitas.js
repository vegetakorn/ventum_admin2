function updateVisita()
{
    var ini = document.getElementById('inputIni').value;
    var fin = document.getElementById('inputFin').value;
    var duracion = document.getElementById('inputDuracion').value;
    if(ini != "")
    {
        if(fin != "")
        {
            if(duracion == "")
            {
                duracion = dur;
            }
            $.ajax({
                method: 'POST',
                url: urlEdit,
                data: {
                    ini:  ini,
                    fin:  fin,
                    duracion: duracion+":00",
                    id: id,
                    _token: csrf
                }
            })
                .done(function(msg){
                    console.log(msg['message']);
                    window.location.replace(urlVis);
                });
        }else
        {
            bootbox.alert("La fecha de fin no puede estar vacia");
        }
    }else
    {
        bootbox.alert("La fecha de inicio no puede estar vacia");
    }
}
function flagActivacion(id)
{
    var visita_id = id
    var text = "";

    /** **/
    bootbox.confirm({
        message: "¿Realmente deseas cancelar la visita?, se borrarán todos los registros referentes a la misma",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true)
            {
                $.ajax({
                    method: 'POST',
                    url: urlDel,
                    data: {id: visita_id, _token: csrf  }
                })
                    .done(function(msg){
                        // console.log(msg['razones']);
                        window.location.replace(urlVis);

                    });
            }else
            {

            }
        }
    });

}