$('#selectEmp').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    // borramos la anterior busqueda
    cleanCombos();
    //alert(document.getElementById('selectEmp').value)
    $.ajax({
        method: 'POST',
        url: urlRaz,
        data: {id:  document.getElementById('selectEmp').value, _token: csrf  }
    })
        .done(function(msg){
            // console.log(msg['razones']);
            $("#selectRaz").append('<option value="0" selected="">Selecciona</option>');
            for(var i = 0; i < msg['razones'].length; i++)
            {
                $("#selectRaz").append('<option value="'+msg['razones'][i]['Id']+'" selected="">'+msg['razones'][i]['nombre']+'</option>');

            }
            $("#selectRaz").val(0);
            $("#selectRaz").selectpicker("refresh");
            $("#selectPla").append('<option value="0" selected="">Selecciona</option>');
            $("#selectPla").selectpicker("refresh");
            $("#selectZon").append('<option value="0" selected="">Selecciona</option>');
            for(var i = 0; i < msg['zonas'].length; i++)
            {
                $("#selectZon").append('<option value="'+msg['zonas'][i]['Id']+'" selected="">'+msg['zonas'][i]['zona']+'</option>');
            }
            $("#selectZon").val(0);
            $("#selectZon").selectpicker("refresh");
        });
});

$('#selectRaz').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    $('select#selectPla').find('option').each(function() {
        //alert($(this).val());
        var option = $(this).val();
        $('#selectPla').find('[value='+option+']').remove();
        $('#selectPla').selectpicker('refresh');
    });

    $.ajax({
        method: 'POST',
        url: urlPla,
        data: {id:  document.getElementById('selectRaz').value, _token: csrf  }
    })
        .done(function(msg){
            // console.log(msg['razones']);
            $("#selectPla").append('<option value="0" selected="">Selecciona</option>');
            for(var i = 0; i < msg['plazas'].length; i++)
            {
                $("#selectPla").append('<option value="'+msg['plazas'][i]['Id']+'" selected="">'+msg['plazas'][i]['plaza']+'</option>');
            }
            $("#selectPla").val(0);
            $("#selectPla").selectpicker("refresh");

        });

});

function cleanCombos()
{
    $('select#selectRaz').find('option').each(function() {
        //alert($(this).val());
        var option = $(this).val();
        $('#selectRaz').find('[value='+option+']').remove();
        $('#selectRaz').selectpicker('refresh');
    });
    $('select#selectZon').find('option').each(function() {
        //alert($(this).val());
        var option = $(this).val();
        $('#selectZon').find('[value='+option+']').remove();
        $('#selectZon').selectpicker('refresh');
    });
    $('select#selectPla').find('option').each(function() {
        //alert($(this).val());
        var option = $(this).val();
        $('#selectPla').find('[value='+option+']').remove();
        $('#selectPla').selectpicker('refresh');
    });
}

function registraTienda()
{

    var empresa = document.getElementById('selectEmp').value;
    var razon = document.getElementById('selectRaz').value;
    var plaza = document.getElementById('selectPla').value;
    var zona = document.getElementById('selectZon').value;
    var clave = document.getElementById('inputClave').value;
    var nombre = document.getElementById('inputName').value;
    var fecha = document.getElementById('inputDate').value;
    var direccion = document.getElementById('inputDireccion').value;
    var emp_ideal = document.getElementById('inputEmp').value;
    var m2 = document.getElementById('inputM2').value;
    var telefono = document.getElementById('inputTelefono').value;
    var email = document.getElementById('inputEmail').value;
    var pass = document.getElementById('inputPass').value;
    var long = document.getElementById('inputLong').value;
    var lat = document.getElementById('inputLat').value;

    if(empresa != 0)
    {
        if(nombre != "")
        {
            if(email != "")
            {

                $.ajax({
                    method: 'POST',
                    url: urlAdd,
                    data: {
                        empresa:  empresa,
                        razon:  razon,
                        plaza:  plaza,
                        zona:  zona,
                        clave:  clave,
                        nombre:  nombre,
                        fecha:  fecha,
                        direccion:  direccion,
                        emp_ideal:  emp_ideal,
                        m2:  m2,
                        telefono:  telefono,
                        email:  email,
                        pass:  pass,
                        long:  long,
                        lat:  lat,
                        _token: csrf
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        if(msg['message'] != 0)
                        {
                            bootbox.alert("El correo electrónico ya existe en sistema, favor de verificar");
                        }else
                        {
                            window.location.replace(urlSuc);
                        }

                    });

            }else
            {
                bootbox.alert("Debes escribir un Correo Electrónico");
            }

        }else
        {
            bootbox.alert("Debes definir un nombre");
        }
    }else
    {
        bootbox.alert("Debes elegir una empresa");
    }


}


