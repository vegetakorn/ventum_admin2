function setFotoEdit()
{
    $avatarImage = $('#avatarImage');
    $avatarInput = $('#avatarInput');
    $avatarForm = $('#avatarForm');
    $rutaFoto = $('.rutaFoto');
    $avatarInput.click();
    $avatarInput.on('change', function () {
        var formData = new FormData();
        formData.append('photo', $avatarInput[0].files[0]);
        $('#modal-info').modal('toggle');

        $.ajax({
            url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
            method: $avatarForm.attr('method'),
            data: formData,
            processData: false,
            contentType: false
        }).done(function (msg) {
            // alert(cam);
            if (data.success)
                $avatarImage.attr('src', data.path);
            var URLdomain = window.location.host;
            var pathImg = 'http://'+URLdomain+'/'+msg['path'];
            $avatarImage.attr('src', pathImg)
            console.log(msg['path']);
            // $('#modal-info').modal('toggle');

            $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['message'] + '" >')


        });
    });
}


function updateEmpresa()
{

    var nombre = document.getElementById('inputNombre').value;
    var direccion = document.getElementById('inputDireccion').value;
    var telefono = document.getElementById('inputTelefono').value;
    var web = document.getElementById('inputWeb').value;
    var logo = document.getElementById('foto_perfil').value;

        if(nombre != "")
        {
                $.ajax({
                    method: 'POST',
                    url: urlUpd,
                    data: {
                        nombre:  nombre,
                        direccion:  direccion,
                        web:  web,
                        telefono:  telefono,
                        id: id,
                        logo: logo,
                        _token: csrf
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        window.location.replace(urlSuc);

                    });
        }else
        {
            bootbox.alert("Debes definir un nombre");
        }

}


function flagActivacion(flag, id)
{
    var activar = flag;
    var tienda_id = id
    var text = "";
    if(flag == 0)
    {
        text = "desactivar";
    }else
    {
        text = "activar";
    }
    /** **/
    bootbox.confirm({
        message: "¿Realmente deseas " + text + " esta empresa?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true)
            {
                $.ajax({
                    method: 'POST',
                    url: urlDel,
                    data: {id: tienda_id, flag: activar, _token: csrf  }
                })
                    .done(function(msg){
                        // console.log(msg['razones']);
                        window.location.replace(urlSuc);

                    });
            }else
            {
            }
        }
    });

}


