function setRazonZona()
{
    // borramos la anterior busqueda
    cleanCombos();
    //alert(document.getElementById('selectEmp').value)
    $.ajax({
        method: 'POST',
        url: urlRaz,
        data: {id:  document.getElementById('selectEmp').value, _token: csrf  }
    })
        .done(function(msg){
            // console.log(msg['razones']);
            for(var i = 0; i < msg['razones'].length; i++)
            {
                $("#selectRaz").append('<option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>');
            }
            $("#selectRaz").val(raz);
            $("#selectRaz").selectpicker("refresh");
            for(var i = 0; i < msg['zonas'].length; i++)
            {
                $("#selectZon").append('<option value="'+msg['zonas'][i]['Id']+'" >'+msg['zonas'][i]['zona']+'</option>');
            }
            $("#selectZon").val(zon);
            $("#selectZon").selectpicker("refresh");

        });
   setPlaza(raz);
}

function setPlaza(raz)
{
    var razon = raz;
    $('select#selectPla').find('option').each(function() {
        //alert($(this).val());
        var option = $(this).val();
        $('#selectPla').find('[value='+option+']').remove();
        $('#selectPla').selectpicker('refresh');
    });
    $.ajax({
        method: 'POST',
        url: urlPla,
        data: {id: razon, _token: csrf  }
    })
        .done(function(msg){
            // console.log(msg['razones']);
            for(var i = 0; i < msg['plazas'].length; i++)
            {
                $("#selectPla").append('<option value="'+msg['plazas'][i]['Id']+'" >'+msg['plazas'][i]['plaza']+'</option>');
            }
            $("#selectPla").val(pla);
            $("#selectPla").selectpicker("refresh");

        });
}

function updateTienda()
{

    var empresa = document.getElementById('selectEmp').value;
    var razon = document.getElementById('selectRaz').value;
    var plaza = document.getElementById('selectPla').value;
    var zona = document.getElementById('selectZon').value;
    var clave = document.getElementById('inputClave').value;
    var nombre = document.getElementById('inputName').value;
    var fecha = document.getElementById('inputDate').value;
    var direccion = document.getElementById('inputDireccion').value;
    var emp_ideal = document.getElementById('inputEmp').value;
    var m2 = document.getElementById('inputM2').value;
    var telefono = document.getElementById('inputTelefono').value;
    var email = document.getElementById('inputEmail').value;
    var pass = document.getElementById('inputPass').value;
    var long = document.getElementById('inputLong').value;
    var lat = document.getElementById('inputLat').value;

    if(empresa != 0)
    {
        if(nombre != "")
        {
            if(email != "")
            {
                $.ajax({
                    method: 'POST',
                    url: urlUpd,
                    data: {
                        empresa:  empresa,
                        razon:  razon,
                        plaza:  plaza,
                        zona:  zona,
                        clave:  clave,
                        nombre:  nombre,
                        fecha:  fecha,
                        direccion:  direccion,
                        emp_ideal:  emp_ideal,
                        m2:  m2,
                        telefono:  telefono,
                        email:  email,
                        pass:  pass,
                        long:  long,
                        lat:  lat,
                        id: id,
                        _token: csrf
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        if(msg['message'] != 0)
                        {
                            bootbox.alert("El correo electrónico ya existe en sistema, favor de verificar");
                        }else
                        {
                            window.location.replace(urlSuc);
                        }
                    });
            }else
            {
                bootbox.alert("Debes escribir un Correo Electrónico");
            }
        }else
        {
            bootbox.alert("Debes definir un nombre");
        }
    }else
    {
        bootbox.alert("Debes elegir una empresa");
    }
}


function flagActivacion(flag, id)
{
    var activar = flag;
    var tienda_id = id
    var text = "";
    if(flag == 0)
    {
        text = "desactivar";
    }else
    {
        text = "activar";
    }
    /** **/
    bootbox.confirm({
        message: "¿Realmente deseas " + text + " esta tienda?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
           if(result == true)
           {
               $.ajax({
                   method: 'POST',
                   url: urlDel,
                   data: {id: tienda_id, flag: activar, _token: csrf  }
               })
                   .done(function(msg){
                       // console.log(msg['razones']);
                       window.location.replace(urlSuc);

                   });
           }else
           {
           }
        }
    });

}


function validaFODA( id)
{

    var tienda_id = id

    $.ajax({
        method: 'POST',
        url: urlFoda,
        data: {id: tienda_id, _token: csrf  }
    })
        .done(function(msg){
            // console.log(msg['razones']);
            window.location.replace(urlSuc);

        });

}